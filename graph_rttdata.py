''' Desc: Given time sent, RTT, RTO, etc. values in the form of a .txt file, graphs data and saves graph locally with appropriate name.
    Params: argv[1] = data file (should be table-formatted, no need for a header), argv[2] = .rtt file from which data came, argv[3] = start line val, argv[4] = end line val
    Author: Arjun Subramonian   '''

import matplotlib.pyplot as pyp
import numpy as np
from sys import argv
import datetime

def strip_first_col(fname, delimiter=None):
    with open(fname, 'r') as fin:
        for line in fin:
            try:
               yield line.split(delimiter, 1)[1]
            except IndexError:
               continue

data = np.loadtxt(strip_first_col(argv[1]))
# data = np.loadtxt(argv[1])[int(argv[2]):int(argv[3]), :]
xlabels = []
inc = data.shape[0] / 10
with open(argv[1], 'r') as f:
	count = 1
	for line in f:
		if count % inc == 0:
			time = datetime.datetime.strptime(line.split()[0], "%Y-%m-%dT%H:%M:%S.%f")
			xlabels.append(60 * time.minute + time.second)
		count += 1
# print len(xlabels)

lines = pyp.plot(data)
pyp.margins(0.01, 0.1)
pyp.legend(lines, ['RTT', 'Machine Learning', 'Model Based', 'Jitter Predictor', 'RWM', 'Jacobson', 'MEP'], loc = 'best')
pyp.xticks(range(0, data.shape[0], inc), xlabels, rotation = 90)
pyp.gcf().subplots_adjust(bottom=0.2, left = 0.2)
pyp.ylabel('RTT')
pyp.savefig('-'.join(argv[2].split('.')[0].split('-')[1:]) + '-' + argv[3] + ':' + argv[4])
