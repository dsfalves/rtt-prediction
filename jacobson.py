# Daniel Alves
# Congestion Avoidance and Control

from rttpredictor import RTTPredictor

class Jacobson(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, alpha=1/8):
        """Initializes the Jacobson algorithm (RFC6298)."""
        RTTPredictor.__init__(self, under_error, over_error)
        self.srtt = None
        self.a = alpha

    def update_srtt(self, rtt, time):
        if self.srtt is None:
            return rtt
        else:
            return (1 - self.a) * self.srtt + self.a * rtt

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)


