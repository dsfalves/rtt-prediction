#Alice Lim
#Rtt prediction based on Holt Winters Method 

from rttpredictor import  RTTPredictor
import numpy as np

class Holt_Winters(RTTPredictor):
    def __init__ (self, under_error=1, over_error=-1, memory = 6):
        RTTPredictor.__init__(self, under_error, over_error)
        self.s = memory
        self.k = 4
        self.lt_curr = 0
        self.bt_curr = 0
        self.st = []
        self.values = []
        self.srtt = None
        self.rttvar = None
        self.a = 0.05
        self.b = 0.05
        self.g = 0.05
        self.alpha = 1./8
        self.beta = 1./4
        self.ready = False

    def update_srtt (self, rtt, time):
        #iterations 1-6 is from Jacobson
        self.values.append(rtt)
        if self.srtt is None:
            self.srtt = rtt
        elif len(self.values) < self.s*2 and not self.ready:
            self.srtt = (1 - self.alpha) * self.srtt + self.alpha * rtt
        elif len(self.values) == self.s*2:
            #self.lt_curr = a[i]
            self.lt_curr = np.sum(self.values[:self.s]) / float(self.s)
            self.bt_curr = (np.sum(self.values[self.s:]) - np.sum(self.values[self.s])) / float(self.s ** 2)
            self.st = [self.values[self.s] / self.lt_curr]
            self.ready = True

        while len(self.values) > self.s+1 and self.ready: 
            self.values.pop(0)
            #if results isn't good, use github code
            lt_old = self.lt_curr
            self.lt_curr = (self.a*(self.values[self.s]/self.st[0]) + (1-self.a) * (self.lt_curr + self.bt_curr)) 
            self.bt_curr = (self.b*(self.lt_curr - lt_old) + (1-self.b) * self.bt_curr)
            self.st.append((self.g*(self.values[self.s]/self.lt_curr)) + (1-self.g) * self.st[0]  )
            self.st.pop(0)

        if self.ready:
            self.srtt = (self.lt_curr + self.bt_curr) * self.st[0]

        return self.srtt

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)

