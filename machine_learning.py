# Arjun Subramonian
# A Machine Learning Approach to End-to-End RTT Estimation and its Application to TCP

from rttpredictor import RTTPredictor
import numpy as np
import math

class Experts(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, n = 7, learning_rate = 1.0, sharing_rate = 0.5):
        RTTPredictor.__init__(self, under_error, over_error)
        self.n = n
        self.learning_rate = learning_rate
        self.sharing_rate = sharing_rate
        self.rtt_min = 1
        self.rtt_max = 128
        self.experts = self.fill_experts()
        self.rttvar = None
        self.weights = np.zeros(n) + 1.0/n
        self.b = 0.25
        self.k = 4

    def fill_experts(self):
        experts = self.rtt_min + self.rtt_max * (2 ** (np.arange(1, self.n+1)/4))
        return experts

    def compute_loss(self, rtt):
        s = self.experts >= rtt
        loss = np.zeros(self.n) + 2*rtt
        loss[s] = (self.experts[s] - rtt)**2
        return loss

    def calc_pool(self, wprime):
        return self.sharing_rate * math.fsum(wprime)

    def update_weights(self, rtt):
        n = int(self.n)
        wprime = self.weights * np.expm1(-self.learning_rate * self.compute_loss(rtt))
        pool = self.calc_pool(wprime)
        self.weights = (1 - self.sharing_rate)*wprime + pool / self.n
        if np.sum(self.weights) < 0.0001:
            self.weights = self.weights / np.sum(self.weights)

    def new_srtt(self):
        return math.fsum(self.weights * self.experts) / math.fsum(self.weights)

    def update_srtt(self, rtt, time):
        self.update_weights(rtt)
        return self.new_srtt()

    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.update_weights(rtt)



