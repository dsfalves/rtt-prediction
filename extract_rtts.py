''' Desc: Given .pcap file(s), prints src, dst, rtt, and time sent of each packet in table format.
    Params: argv[1:] = .pcap file(s)    
    Author: Daniel Alves    '''

from sys import argv
from parse import process_pcap
from fileinput import input
import netaddr
from datetime import datetime

class RTTCalculator(object):
    def __init__(self, src, dst):
        self.src = src
        self.dst = dst
        self.times = {}

    def process(self, pkt):
        src = pkt.src
        dst = pkt.dst
        #import pdb; pdb.set_trace()
        if 'options' not in pkt.extra or 'TS' not in pkt.extra['options']:
            return
        if dst == self.dst:
            try:
                ts = pkt.extra['options']['TS']['val']
            except KeyError:
                return
            if ts not in self.times:
                self.times[ts] = pkt.time
        else:
            try:
                ts = pkt.extra['options']['TS']['ecr']
            except AttributeError:
                return
            return (pkt.time - self.times[ts], self.times[ts])

def starting(pkt):
    if 'syn' in pkt.flags and 'ack' not in pkt.flags:
        return True
    return False
    
# this doesn't work in the general case, but for me sources and destinations
# will never exchange roles
def get_pair(src, dst, sources, pkt):
    if starting(pkt):
        sources.add(src)
    if src in sources:
        return "%d-%d" % (src, dst)
    if dst in sources:
        return "%d-%d" % (dst, src)

calculators = {}
sources = set()
for file in argv[1:]:
    for pkt in process_pcap(file):
        if pkt is None:
            continue
        src = pkt.src
        dst = pkt.dst
        pair = get_pair(src, dst, sources, pkt)
        if pair is not None:
            if pair not in calculators:
                if starting(pkt):
                    calculators[pair] = RTTCalculator(src, dst)
                else:
                    continue
            ret = calculators[pair].process(pkt)
            if ret is not None:
                print src, dst, ret[0] * 1000, datetime.utcfromtimestamp(ret[1]).isoformat()
