# Arjun Subramonian
# TCP Retransmission Timer Adjustment Mechanism Using Model-Based RTT Predictor

from rttpredictor import RTTPredictor
from jacobson import Jacobson
import datetime
import numpy as np

class ModelBased(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, lamb = 0.97, na = 15, nb = 15):
        RTTPredictor.__init__(self, under_error, over_error)
        self.lamb = lamb
        self.hist = []
        self.lastP = 0
        self.lastTheta = 0
        self.phi = np.matrix('')

        self.na = na
        self.nb = nb

        self.jacobson = Jacobson()

        self.count = 0

    def u(self, k):
        if k <= 0:
            return 0
        try:
            return (self.hist[k][1] - self.hist[k - 1][1]).total_seconds()
        except AttributeError:
            return self.hist[k][1] - self.hist[k-1][1]

    def deltay(self, k):
        if k <= 0:
            return 0
        return self.hist[k][0] - self.hist[k - 1][0]

    def deltau(self, k):
        return self.u(k) - self.u(k - 1)

    def phi_initial(self, k):
        phi = []
        timer = 1
        while timer <= self.na:
            phi.append(-self.deltay(k - timer))
            timer += 1
        timer = 1
        while timer <= self.nb:
            phi.append(self.deltau(k - timer))
            timer += 1
        mat = np.matrix(phi)
        return mat.T

    def update_phi(self, k):
        temp1 = np.vstack((self.phi[0:self.na - 1], -self.deltay(k - 1)))
        temp2 = np.vstack((self.phi[self.na:-1], self.deltau(k - 1)))
        return np.vstack((temp1, temp2))

    def P(self, k):
        phi = self.phi
        pminus = self.lastP
        try:
            return (1 / self.lamb) * (pminus - (pminus * phi.T.dot(phi).item(0) * pminus) / (self.lamb + (phi.T * pminus).dot(phi).item(0)))
        except AttributeError:
            import pdb; pdb.set_trace()

    def L(self, k):
        phi = self.phi
        pminus = self.lastP
        return pminus * phi / (self.lamb + (phi.T * pminus).dot(phi).item(0))

    def theta(self, k):
        tminus = self.lastTheta
        tcurr = tminus + self.L(k).dot(self.deltay(k) - self.phi.T.dot(tminus))
        self.lastTheta = tcurr
        return tcurr

    def deltayhat(self, k):
        coeffs = np.ravel(self.theta(k).T)
        acoeffs = coeffs[0:coeffs.size / 2]
        bcoeffs = coeffs[coeffs.size / 2:]
        res = 0
        sub = 0
        while sub <= acoeffs.size - 1:
            res += -acoeffs[sub] * self.deltay(k - sub)
            sub += 1
        sub = 0
        while sub <= bcoeffs.size - 1:
            res += bcoeffs[sub] * self.deltau(k - sub)
            sub += 1
        return res

    def yhat(self, k):
        return self.hist[k][0] + self.deltayhat(k)

    def update_srtt(self, rtt, time):
        self.hist.append((rtt, time))
        k = self.count
        if k == 0:
            self.lastP = 1
            self.lastTheta = 1
            self.count += 1
            return self.jacobson.update_srtt(rtt, time)
        if k >= max(self.na, self.nb):
            self.phi = self.update_phi(k)
            self.srtt = self.yhat(k)
        elif k < max(self.na, self.nb):
            if k == 1:
                self.phi = self.phi_initial(k)
            else:
                self.phi = self.update_phi(k)
            self.srtt = self.jacobson.update_srtt(rtt, time)
        self.lastP = self.P(k)
        self.count += 1
        return self.srtt


    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
        self.hist.append((rtt, time))
        k = self.count
        if k == 0:
            self.lastP = 1
            self.lastTheta = 1
            self.count += 1
            return
        if k >= max(self.na, self.nb):
            self.phi = self.update_phi(k)
        elif k < max(self.na, self.nb):
            if k == 1:
                self.phi = self.phi_initial(k)
            else:
                self.phi = self.update_phi(k)
        self.lastP = self.P(k)
        self.count += 1

