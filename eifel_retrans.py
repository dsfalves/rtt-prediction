#Alice Lim
#Eifel 
#alternitive timers fo the transmission protocall (TCP)

from rttpredictor import  RTTPredictor

#"the following set of equations define RTOL (L = lite)"

class Eifel(RTTPredictor):
    def __init__ (self, under_error=1, over_error=-1):
        RTTPredictor.__init__(self, under_error, over_error)
        self.gain = 1.0/3.0
        self.srtt = None
        self.rttvar = None
        self.delta = None
        self.gainbar = None

    def update_srtt (self, rtt, time):
        if self.srtt is None:
                self.srtt = rtt
        else:
                #delta starts calculating after the third iteration 
                self.delta = rtt - self.srtt 
                self.srtt = self.srtt + self.gain*self.delta

                #calculate the gain bar based on certain conditions
                if self.delta - self.rttvar >= 0:
                        self.gainbar = self.gain
                else: 
                        self.gainbar = self.gain**2.0
        return self.srtt

    def update_rttvar(self, rtt, time):
        #calculate rttvar based on certain conditions
        if self.rttvar == None:
            return rtt/2
        elif self.delta < 0:
                self.rttvar = self.rttvar
        else: 
                self.rttvar = self.rttvar + self.gainbar*(self.delta - self.rttvar)
        return self.rttvar

    def update_rto(self, rtt, time):
        #recalcuate rto because now you have delta 
        self.rto = max((self.srtt + 1.0/self.gain*self.rttvar), rtt + 2*self.granularity)
        return self.rto



    def process_loss(self, rtt, time):
        RTTPredictor.process_loss(self, rtt, time)
