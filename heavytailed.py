from rttpredictor import RTTPredictor
import numpy as np 
import math 
import bisect

#This algorithm uses the Cachy distribution to predict future rtts using previous rtt values
#Ishani Karmarkar
class Cauchy(RTTPredictor):
    def __init__(self, under_error=1, over_error=-1, alpha=1/8, beta=1/4, error = 1/8):
        RTTPredictor.__init__(self, under_error, over_error)
        self.last_rtt = 1000
        self.srtt = None
        self.rttvar = None
        self.rttStore = None
        self.k = 4
        self.b = beta
        self.a = alpha
        self.e = error
        self.tracker = False
        self.history = [ ]
        self.alphaMat = np.matrix([
            [2.400, 2.000, 2.000, 2.000, 2.000, 2.000, 2.000, 2.000], 
            [2.500, 1.916, 1.924, 1.924, 1.924, 1.924, 1.924, 1.924],
            [2.600, 1.808, 1.813, 1.829, 1.829, 1.829, 1.829, 1.829],
            [2.700, 1.729, 1.730, 1.737, 1.745, 1.745, 1.745, 1.745],
            [2.800, 1.664, 1.663, 1.663, 1.668, 1.676, 1.676, 1.676],
            [2.900, 1.563, 1.560, 1.553, 1.548, 1.547, 1.547, 1.547],
            [3.000, 1.563, 1.560, 1.553, 1.548, 1.547, 1.547, 1.547],
            [3.100, 1.484, 1.480, 1.471, 1.460, 1.448, 1.438, 1.438],           
            [3.200, 1.484, 1.480, 1.471, 1.460, 1.448, 1.438, 1.438],
            [3.300, 1.484, 1.480, 1.471, 1.460, 1.448, 1.438, 1.438],
            [3.400, 1.391, 1.386, 1.378, 1.364, 1.337, 1.318, 1.318],
            [3.500, 1.391, 1.386, 1.378, 1.364, 1.337, 1.318, 1.318],

            [4.000, 1.279, 1.273, 1.266, 1.250, 1.210, 1.184, 1.150],
            [5.000, 1.128, 1.121, 1.114, 1.101, 1.067, 1.027, 0.973],
            [6.000, 1.029, 1.021, 1.014, 1.004, 0.974, 0.935, 0.874],
            [7.000, 0.896, 0.892, 0.887, 0.883, 0.855, 0.823, 0.769],
            [8.000, 0.896, 0.892, 0.887, 0.883, 0.855, 0.823, 0.769],
            [9.000, 0.818, 0.812, 0.806, 0.801, 0.780, 0.756, 0.691],
            [10.00, 0.818, 0.812, 0.806, 0.801, 0.780, 0.756, 0.691],

            [15.00, 0.698, 0.695, 0.692, 0.689, 0.676, 0.656, 0.595],
            [25.00, 0.593, 0.590, 0.588, 0.586, 0.579, 0.563, 0.513]])

        self.betaMat = np.matrix([
            [2.400, 0.000, 2.160, 1.000, 1.000, 1.000, 1.000, 1.000],
            [2.500, 0.000, 1.592, 3.390, 1.000, 1.000, 1.000, 1.000],
            [2.600, 0.000, 0.759, 1.800, 1.000, 1.000, 1.000, 1.000],
            [2.700, 0.000, 0.482, 1.048, 1.694, 1.000, 1.000, 1.000],
            [2.800, 0.000, 0.360, 0.760, 1.232, 2.229, 1.000, 1.000],
            [2.900, 0.000, 0.253, 0.518, 0.823, 1.575, 1.000, 1.000],
            [3.000, 0.000, 0.253, 0.518, 0.823, 1.575, 1.000, 1.000],
            [3.100, 0.000, 0.203, 0.410, 0.632, 1.244, 1.906, 1.000],
            [3.200, 0.000, 0.203, 0.410, 0.632, 1.244, 1.906, 1.000],
            [3.300, 0.000, 0.203, 0.410, 0.632, 1.244, 1.906, 1.000],
            [3.400, 0.000, 0.165, 0.332, 0.499, 0.943, 1.560, 1.000],
            [3.500, 0.000, 0.165, 0.332, 0.499, 0.943, 1.560, 1.000],

            [4.000, 0.000, 0.136, 0.271, 0.404, 0.689, 1.230, 1.150],
            [5.000, 0.000, 0.109, 0.216, 0.323, 0.539, 0.827, 0.973],
            [6.000, 0.000, 0.096, 0.190, 0.284, 0.472, 0.693, 0.874],
            [7.000, 0.000, 0.082, 0.163, 0.243, 0.412, 0.601, 0.769],
            [8.000, 0.000, 0.082, 0.163, 0.243, 0.412, 0.601, 0.769],
            [9.000, 0.000, 0.074, 0.147, 0.220, 0.377, 0.546, 0.691],
            [10.00, 0.000, 0.074, 0.147, 0.220, 0.377, 0.546, 0.691],

            [15.00, 0.000, 0.064, 0.128, 0.191, 0.330, 0.478, 0.595],
            [25.00, 0.000, 0.056, 0.112, 0.167, 0.285, 0.438, 0.513]])

        self.cMat = np.matrix([
            [2.000, 1.908, 1.908, 1.908, 1.908, 1.908],
            [1.900, 1.914, 1.915, 1.916, 1.918, 1.921],
            [1.800, 1.921, 1.922, 1.927, 1.936, 1.947],
            [1.700, 1.927, 1.930, 1.943, 1.961, 1.987],
            [1.600, 1.933, 1.940, 1.962, 1.997, 2.043],
            [1.500, 1.939, 1.952, 1.988, 2.045, 2.116],

            [1.400, 1.946, 1.967, 2.022, 2.106, 2.211],
            [1.300, 1.955, 1.984, 2.067, 2.188, 2.333],
            [1.200, 1.965, 2.007, 2.125, 2.294, 2.491],
            [1.100, 1.980, 2.040, 2.205, 2.435, 2.696],
            [1.000, 2.000, 2.085, 2.311, 2.624, 2.973],

            [0.900, 2.040, 2.149, 2.461, 2.886, 3.356],
            [0.800, 2.098, 2.244, 2.676, 2.265, 3.912],
            [0.700, 2.189, 2.392, 3.004, 3.844, 4.775],
            [0.600, 2.337, 2.635, 3.542, 4.808, 9.144]])

    #calculates the predicted value of va 
    def va (self):
        arr = sorted(self.history)
        x95 = (arr[19] + arr[18])/2
        x75 = (arr[15] + arr[14])/2
        x25 = (arr[5] + arr[4])/2
        x05 = (arr[1] + arr[0])/2
        va = (x95-x05)/(x75-x25)
        #round va
        if va <= 2.400: 
            va = 2.400
        elif va <= 3.500:
            va = round(va, 1)
        elif va <= 10.000: 
            va = int(va)
        elif va >= 10 and va < 15: 
            va = 10
        elif va >= 15 and va < 20: 
            va = 15
        else: 
            va = 25 
        return va 

    #calculates the predicted value of vb 
    def vb (self): 
        arr = sorted(self.history)
        x95 = (arr[19] + arr[18])/2
        x50 = (arr[10] + arr[9])/2
        x05 = (arr[1] + arr[0])/2
        vb = (x95 + x05 - 2(x50))/(x95 - x05)
        #round vb
        if vb <= 0.0: 
            vb = 0.0
        elif vb <= 0.3: 
            vb = round(vb, 1)
        elif vb < 0.4: 
            vb = 0.3
        elif vb < 0.5: 
            vb = 0.5
        elif vb < 0.6: 
            vb = 0.5
        elif vb < 0.7: 
            vb = 0.7
        elif vb < .85: 
            vb = .7
        else: 
            vb = 1
        return vb

    #uses va and vb to calculate a
    def a(self): 
        lista = [2.400, 2.500, 2.600, 2.700, 2.800, 2.900, 3.000, 3.100, 3.200, 3.300, 3.400, 3.500, 4.000, 5.000, 6.000, 
        7.000, 8.000, 9.000, 10.000, 15.000, 25.000]
        row = lista.index(self.va) 
        listb = [0.0, 0.1, 0.2, 0.3, 0.5, 0.7, 1.0]
        col = listb.index(self.vb) + 1
        a = self.alphaMat[row][col]
        #round a
        if a > 2.000: 
            a = 2.00
        if a <= 2.000 and a >= 0.500: 
            a = round(a, 1)
        else: 
            a = 0.500
        return a

    #uses va and vb to calculate b 
    def b(self):
        lista = [2.400, 2.500, 2.600, 2.700, 2.800, 2.900, 3.000, 3.100, 3.200, 3.300, 3.400, 3.500, 4.000, 5.000, 6.000,
        7.000, 8.000, 9.000, 10.000, 15.000, 25.000]
        row = lista.index(self.va) 
        listb = [0.0, 0.1, 0.2, 0.3, 0.5, 0.7, 1.0]
        col = listb.index(self.vb) + 1
        b = self.betaMat[row][col]
        #round b
        if b < 0.125: 
            b = 0 
        elif b < 0.375: 
            b = .25
        elif b < 0.625: 
            b = .5
        elif b < 0.875: 
            b = .75
        else: 
            b = 1
        return b
    #uses a and b to calculate vc 

    def vc(self): 

        lista = [2.000, 1.900, 1.800, 1.700, 1.600, 1.500, 1.300, 1.200, 1.100, 1.000, .900, .800, .700, .600, .500]
        row = lista.index(self.a)

        listb = [0.0, 0.25, 0.50, 0.75, 1.0]
        col = listb.index(self.b) + 1

        vc = self.cMat[row][col]
        return vc
    #uses vc to calculate c OR implements jacobson

    def c(self, rtt): 
        #implement jacobson's for the first 10 rtt predictions 
        if k < 20: 
            if self.srtt is None:
                self.srtt = rtt
                self.rttvar = rtt/2
                self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
            else:
                self.rttvar = (1 - self.b) * self.rttvar + (
                self.b * abs(self.srtt - rtt))
                self.srtt = (1 - self.a) * self.srtt + self.a * rtt
                self.rto = self.srtt + max(self.granularity,
                                       self.k * self.rttvar)
            if self.rto < 1000:
                self.rto = 1000
            self.rttStore = rtt  
            self.history.append[rtt]
            self.history.pop(0)   
            return 
        #implement the chacy distribution algorithm for all k>10 
        else: 
            arr = sorted(self.history)
            x75 = (arr[15] + arr[14])/2
            x25 = (arr[5] + arr[4])/2
            c = (x75 - x25)/(self.vc)
            self.rttStore = rtt
            self.history.append[rtt]
            self.history.pop(0)
            self.Tracker = True
        return c

    def update_srtt(self, rtt, time):
        self.srtt = rtt
        return self.srtt

    def update_rto(self, rtt, time):
        if self.tracker == False: 
            return self.rto
        else:
            term = (2 * self.c(self.srtt) * self.error) / (math.tan(self.c(self.srtt) * math.pi) + (self.error ** 2) - (self.c(self.srtt) ** 2))
            self.rto = self.rttStore + math.sqrt(term)
        return self.rto
