#!/usr/bin/python

''' Desc: For testing purposes only - plots correct RTT on same cartesian plane as RWM's respective RTO in order to diagnose high loss.
    Params: argv[1:] = .rtt file(s)
    Author: Arjun Subramonian   '''

from weightedmedians import RWM

from sys import argv
import fileinput
import datetime
import matplotlib.pyplot as pyp

if __name__ == "__main__":
    rtos = []
    rtts = []
    p = RWM()
    count = 0
    for line in fileinput.input(argv[1:]):
        arr = line.strip().split()
        rtt = float(arr[0])
        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        p.receive_rtt(rtt, time)
        rtos.append(p.rto)
        rtts.append(rtt)
        count += 1
    print type(p).__name__, p.lost_packets*100.0/count,

    pyp.hist(rtos, bins = 100)    
    pyp.show()

    pyp.hist(rtts, bins = 100)
    pyp.show()

    print rtos
    # pyp.plot(rtos)
    pyp.plot(rtts)
    pyp.show()
