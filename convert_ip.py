''' Desc: Simply prints understandable IP Address given a connection (filters out .rtt if necessary).
    Params: argv[1] = connection    
    Author: Arjun Subramonian'''

from sys import argv
import netaddr as na

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# print argv[1]
(src, dst) = [na.IPAddress(elem).__str__() for elem in argv[1].split('-') if is_number(elem)]
print '\n\n' + src + ' > ' + dst