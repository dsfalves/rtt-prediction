''' Deprecated
    Author: Arjun Subramonian   '''

from fileinput import input
from sys import argv
import collections

'''dict = {}
for line in input(argv[1]):
    dict[line.strip().split(':')[0]] = [line.strip().split(':')[1]]

count = 0
flag = False
key = None
for line in input(argv[2]):
    line = line.strip()
    if line in dict:
        key = line
        flag = True
    elif flag == True and len(line.strip()) != 0:
        dict[key].append(line.split()[1])
        count += 1
        if count == int(argv[3]):
            count = 0
            flag = False

for key in dict:
    if len(dict[key]) == 1:
        continue
    print '{:25s}'.format(key),
    for elem in reversed(dict[key]):
        print '{:20s}'.format(elem),
    print'''

#   Connection      Jacobson        RWM     JitterPredictor     ModelBased      MachineLearning     ActualLoss      

dict = collections.OrderedDict()
count = 0
flag = False
key = None
for line in input(argv[1]):
    line = line.strip()
    if len(line) == 0:
        continue
    if flag == False:
        dict[line] = []
        key = line
        flag = True
    elif flag == True:
        dict[key].append(line.split()[1])
        count += 1
        if count == int(argv[2]):
            count = 0
            flag = False

for key in dict:
    print '{:25s}'.format(key),
    for elem in reversed(dict[key]):
        print '{:20s}'.format(elem),
    print
