#!/usr/bin/env python
# coding: utf-8

''' Desc:
    Author: Daniel Alves    '''

from re import compile
from fileinput import input, filename
from subprocess32 import Popen, PIPE
import netaddr

class Packet(object):

    def __init__(self, time, addresses, flags, extra):
        self.time = time
        self.src = netaddr.IPAddress(addresses[0])
        self.dst = netaddr.IPAddress(addresses[1])
        if 'options' in extra:
            if 'TS' in extra['options']:
                self.ecr = extra['options']['TS']['ecr']
                self.val = extra['options']['TS']['val']
        if 'seq' in extra:
            self.seq = extra['seq']
        if 'ack' in flags and 'ack' in extra:
            self.ack = extra['ack']
        self.flags = flags
        self.extra = extra
        self.length = extra['length']

    def copy(self):
        p = Packet(self.time, (str(self.src), str(self.dst)),
                   self.flags, self.extra)
        return p


ip = r'\d+(?:\.\d+){3}'
header = compile(r'(\d+\.\d+) IP (%s)\.\S+ > (%s)\.\S+: Flags \[(.*?)\],(.*)' %
                 (ip, ip))
extra_pattern = compile(r'(\w+) (\S+|\[.*\])(?:,|$)')

flags_dict = {
    'S': 'syn',
    '.': 'ack',
    'P': 'push',
    'R': 'reset',
    'F': 'fin',
    }

def process_line(line):
    l = line.strip()
    result = header.match(l)
    if not result:
        if 'IP' not in line:
            return
        raise ValueError
    time, src, dst, flags_bits, other = result.groups()
    time = float(time)
    flags = set()
    for f in flags_bits:
        if f in flags_dict:
            flags.add(flags_dict[f])
        else:
            raise ValueError
    extra = {}
    for key, element in extra_pattern.findall(other):
        if element.startswith('['):
            extra[key] = {}
            for node in element[1:-1].split(','):
                node = node.strip()
                if node.startswith('eol'):
                    extra[key]['eol'] = 1
                elif node.startswith('TS'):
                    blocks = node.split()[1:]
                    extra[key]['TS'] = {}
                    for i in xrange(len(blocks)/2):
                        extra[key]['TS'][blocks[i*2]] = int(blocks[i*2+1])
        else:
            if key == 'seq':
                if ':' in element:
                    element = element.split(':', 1)[0]
            try:
                element = int(element)
            except ValueError:
                try:
                    element = float(element)
                except ValueError:
                    pass
            extra[key] = element
    return (time, (src, dst), flags, extra)

def process_pcap(pcapname):
    pipe = Popen(["tcpdump", "-r", pcapname, "-tt", "-n"], stdout=PIPE).stdout
    for line in pipe:
        yield Packet(*process_line(line))

if __name__ == "__main__":
    for line in input():
        (time, addresses, flags, extra) = process_line(line)
        print time, addresses, flags, extra



