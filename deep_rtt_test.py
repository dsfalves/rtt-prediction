from parse import process_pcap
import netaddr
import heapq
from subprocess32 import Popen, PIPE
from datetime import datetime
from sys import argv
from machine_learning import MachineLearning
from model_based import ModelBased
from Mep import MEP
from jacobson import Jacobson
from holt_winters import Holt_Winters
from eifel_retrans import Eifel
from heavytailed import Cauchy
from weightedmedians import RWM


class Calculator(object):

    def __init__(self, predictor):
        self.predictor = predictor
        self.pending = []
        self.missed = set()
        self.times = {}
        self.rtt_map = {}
        self.error = 0
        self.retrans = 0
        self.expiring = {}

    def receive(self, pkt):
        # TODO this will cause some errors, and I will have to fix
        # but only if using with general pcap files, files
        # from simulation are safe
        if 'syn' in pkt.flags and 'ack' not in pkt.flags:
            return
        if ((pkt.ecr, pkt.ack) in self.missed):
            self.retrans += 1
            return
        time = self.times[pkt.ecr]
        while len(self.pending) and self.pending[0] <= pkt.ack:
            heapq.heappop(self.pending)
        rtt = pkt.time - time
        self.predictor.update_rto(rtt, pkt.time)
        #if self.rtt_map[pkt.ack] - rtt < 0:
            #import pdb; pdb.set_trace()
        try:
            self.error += self.rtt_map[pkt.ack][0] - rtt
            self.agerror += self.rtt_map[pkt.ack][1] - rtt
        except KeyError:
            self.error += self.rtt_map[pkt.ack-1][0] - rtt
            self.agerror += self.rtt_map[pkt.ack-1][1] - rtt

    def send(self, queue, pkt):
        if 'reset' in pkt.flags:
            return
        try:
            s = pkt.seq
        except AttributeError as e:
            return # no sequence
        s += pkt.length
        heapq.heappush(self.pending, s)
        try:
            self.times[pkt.val] = pkt.time
        except:
            import pdb; pdb.set_trace()
        self.expiring[pkt.val] = pkt.time + self.predictor.rto
        heapq.heappush(queue, (pkt.time + self.predictor.rto, pkt,
                               self.expire))
        if s == 0:
            s = 1
        self.rtt_map[s] = (self.predictor.rto, self.predictor.srtt)

    def expire(self, queue, pkt):
        seq = pkt.seq + pkt.length
        if seq not in self.pending:
            return
        # TODO remove parameters from this function
        self.missed.add((pkt.val, pkt.seq + pkt.length))
        self.predictor.process_loss(0, 0)


class Controller(object):

    def __init__(self, filename, predictors):
        self.connection = {}
        self.sources = set()
        self.queue = []
        self.filename = filename
        self.predictors = predictors

    # this doesn't work in the general case, but for me sources and destinations
    # will never exchange roles
    def get_pair(self, pkt):
        if starting(pkt):
            self.sources.add(pkt.src)
        if pkt.src in self.sources:
            return "%d-%d" % (pkt.src, pkt.dst)
        if pkt.dst in self.sources:
            return "%d-%d" % (pkt.dst, pkt.src)

    def process_file(self):
        count = 0.0
        for pkt in process_pcap(self.filename):
            pkt.time = pkt.time * 1000 # convert seconds to milliseconds
            #print "process pkt: %f - %d -> %d" % (pkt.time, pkt.src, pkt.dst)
            if starting(pkt):
                #print "starter pkt: %d -> %d" % (pkt.src, pkt.dst)
                self.connection[self.get_pair(pkt)] = [
                    Calculator(p()) for p in self.predictors]
                #import pdb; pdb.set_trace()
                p = pkt.copy()
                heapq.heappush(self.queue, (pkt.time, p, self.send))
                count += 1
            else:
                p = pkt.copy()
                if self.get_pair(pkt) == "%d-%d" % (pkt.src, pkt.dst):
                    #print "pkt: cv w%d -> %d" % (pkt.src, pkt.dst)
                    heapq.heappush(self.queue, (pkt.time, p, self.send))
                    count += 1
                else:
                    #print "pkt: %d -> %d" % (pkt.dst, pkt.src)
                    heapq.heappush(self.queue, (pkt.time, p, self.receive))

        while len(self.queue) > 0:
            p = heapq.heappop(self.queue)
            try:
                func = p[2]
            except:
                import pdb; pdb.set_trace()
            func(self.queue, p[1])

        return [(k, [(type(calc.predictor).__name__,
                      calc.retrans / float(count),
                      calc.error / float(count),
                      calc.agerror / float(count)
                 for calc in self.connection[k]])
                for k in self.connection.keys()]

    def receive(self, q, pkt):
        connection = self.get_pair(pkt)
        for c in self.connection[connection]:
            c.receive(pkt)

    def send(self, q, pkt):
        connection = self.get_pair(pkt)
        for c in self.connection[connection]:
            c.send(q, pkt)


def starting(pkt):
    if 'syn' in pkt.flags and 'ack' not in pkt.flags:
        return True
    return False


def main():
    predictors = [
        MachineLearning,
        ModelBased,
        #JitterPredictor(open(argv[1], "r")),
        RWM,
        MEP,
        Eifel,
        Holt_Winters,
        Jacobson,
        Cauchy,
    ]

    for filename in argv[1:]:
        c = Controller(filename, predictors)
        res = c.process_file()
        for k, calcs in res:
            for name, loss, error, agerror in calcs:
                print filename, k, name, loss, error, agerror

if __name__ == '__main__':
    main()
