#Alice Lim
#end-to-end algorithm for RTT prediction based on INFORMATION THEORY 
#implements MEP (Maximum Entropy Principle)

from scipy.optimize import fsolve 
from rttpredictor import RTTPredictor
import numpy as np
import math as m 

#solvig for Beta (6)

class MEP(RTTPredictor):
    def __init__ (self, under_error=1, over_error=-1, memory = 4):
        RTTPredictor.__init__(self, under_error, over_error)
        self.memory = memory
        self.values = []
        self.srtt = 0
        self.rttvar = 0
        

    def update_srtt(self, rtt, time):
        #start implementing (11)
        self.values.append(rtt)
       
        y = 0.2

        #find the lowest value (not stored)
        #find the highest value (not stored)

        lowest = (1-y) * min(self.values)                            
        highest = (1+y) * max(self.values)                            
        #all_values = appended RTT data and added low and high value
        self.all_values = np.array(self.values + [lowest, highest])           
        
        beta = fsolve(self.equation11, 0)
        beta = beta[0]

        if len(self.values) > self.memory:
            self.all_values = self.all_values[1:]
            self.values.pop(0)


        #start new equation to update coefficient (10)
        #top = top of equationprint
        self.top = np.exp(-beta*self.all_values)
        coefficient = self.top/np.sum(self.top)


        #figure out the SRT
        #set conditions to implement (6)
        self.srtt = np.sum(coefficient * self.all_values)
        return self.srtt


    def equation11(self, b):
        c = np.append(self.all_values[:-3], self.all_values[-2:]) 
        v = c-self.all_values[-3]                             
        return np.sum(v*np.exp(-b*v))

