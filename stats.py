''' Desc: Simply for testing purposes - gets percentiles, average, and median of all .rtt files passed in.
    Params: .rtt files
    Author: Arjun Subramonian   '''

import fileinput
import numpy as np
import matplotlib.pyplot as pyp

'''rtts = [np.log(float(line.strip().split()[0])) for line in input()]
print "average:", np.mean(rtts)
perc = [0, 25, 50, 75, 100]
res = ["q" + str(val) + ": " + str(np.percentile(rtts, val)) for val in perc]
for val in res:
    print val
print "std: " + str(np.std(rtts)) + "\n"

pyp.hist(rtts, bins = 100)
pyp.show()'''

rtts = {'2-2-2-0':[], 
        '2-2-2-3':[], 
        '2-8-2-0':[]}
dict = {'2-2-2-0':[[] for i in range(0, 5)], 
        '2-2-2-3':[[] for i in range(0, 5)], 
        '2-8-2-0':[[] for i in range(0, 5)]}
total = [[] for i in range(0, 5)]
for line in fileinput.input():
    code = fileinput.filename().split('/')[0]
    # print fileinput.filename()
    rtt = float(line.strip().split()[0])
    rtts[code].append(rtt)

for key in rtts:
    dict[key][0].append(np.mean(rtts[key]))
    total[0].append(np.mean(rtts[key]))
    perc = [0, 25, 50, 75]
    for count in range(1, 5):
        dict[key][count].append(np.percentile(rtts[key], perc[count - 1]))
        total[count].append(np.percentile(rtts[key], perc[count - 1]))

# print dict

print len(total)

handles = []
labels = ['mean', 'q0', 'q25', 'q50', 'q75']
for count in range(0, len(total)):
    handles.extend(pyp.plot(total[count], label = labels[count]))
print handles
pyp.legend(handles = handles)
pyp.show()





