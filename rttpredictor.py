''' Desc: Parent class of predictors.
    Author: Daniel Alves (Modified: Arjun Subramonian)  '''
    
import datetime

class RTTPredictor(object):
    def __init__(self, under_error = 1, over_error = -1, min_rto = 1000, b=0.25, k=4):
        """Initiates the algorithm's parameters with default values"""
        self.under_error = under_error
        self.over_error = over_error
        self.min_rto = min_rto
        self.rto = 1000
        self.srtt = None
        self.rttvar = None
        self.error = 0
        self.agerror = 0
        self.lost_packets = 0
        # timer granularity in milliseconds
        self.granularity = 100
        self.b = b
        self.k = k
        
    def receive_rtt(self, rtt, time):
        """Receive the next measured RTT and update internal parameters."""
        d = self.rto - rtt
        if d < 0:
            # if it's the first time and there isn't a set srtt yet
            if self.srtt is not None and self.error == 0:
                self.agerror += (self.over_error * self.srtt - rtt)**2
            self.error += (self.over_error * d)**2
            self.process_loss(rtt, time)
        else:
            self.error += (self.under_error * d)**2
            if self.srtt is not None:
                self.agerror += (self.under_error * self.srtt - rtt)**2
            self.rttvar = self.update_rttvar(rtt, time)
            self.srtt = self.update_srtt(rtt, time)
            self.rto = self.update_rto(rtt, time)

    def update_srtt(self, rtt, time):
        print "Warning: this shouldn't be visible"
        return rtt

    def update_rttvar(self, rtt, time):
        if self.rttvar == None:
            return rtt/2
        return (1-self.b) * self.rttvar + self.b * abs(self.srtt-rtt)

    def update_rto(self, rtt, time):
        try:
            return max(self.srtt + self.k * self.rttvar, 2*self.granularity)
        except TypeError:
            import pdb; pdb.set_trace()

    def process_loss(self, rtt, time):
        self.lost_packets += 1
        self.rto = min(2*self.rto, 60*self.granularity)

