#!/usr/bin/python

''' Desc: Runs through all predictors, and calculates number of lost packets and error using actual RTTs and their predicted values.
    Params: argv[1] = training file for Jitter, argv[2:] = test files
    Author: Daniel Alves  '''

from machine_learning import Experts
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM
from Mep import MEP
from eifel_retrans import Eifel
from holt_winters import Holt_Winters
from machine_learning_new import Sense
from heavytailed import Cauchy

from sys import argv
import fileinput
import datetime

class Tester:
    def __init__(self):
        self. predictors = [
            Experts(), 
            ModelBased(), 
            #JitterPredictor(open(argv[1], "r")),
            RWM(),
            MEP(),
            Eifel(),
            Holt_Winters(),
            #Sense(),
            Jacobson(), 
            Cauchy(),
        ]
        self.count = 0

    def get_results(self):
        results = []
        if self.count > 0:
            for p in self.predictors:
                results.append("%s %s %s %s" % (
                    type(p).__name__,
                    p.lost_packets*100.0/self.count,
                    p.error/self.count,
                    p.agerror/self.count))

        return results

    def process_line(self, line):
        arr = line.strip().split(" ", 1)
        rtt = float(arr[0])
        try:
            time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
        except ValueError:
            try:
                time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S")
            except ValueError:
                try:
                    time = datetime.datetime.strptime(arr[-1], "%Y-%m-%d %H:%M:%S.%f")
                except ValueError:
                    try:
                        time = datetime.datetime.strptime(arr[-1], "%Y-%m-%d %H:%M:%S")
                    except ValueError:
                        time = float(arr[-1])
        for p in self.predictors:
            p.receive_rtt(rtt, time)
            self.count += 1
    
    def main(self):
        for line in fileinput.input(argv[1:]):
            self.process_line(line)

        print '\n'.join(self.get_results())


if __name__ == "__main__":
    t = Tester()
    t.main()
