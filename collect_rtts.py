#!/usr/bin/python

''' Desc: Given a training file (for Jitter) and a test file (from the same connection), prints connection, RTT, and predicted RTTs
    for different predictors to a txt file in table format.
    Params: argv[1] = training file, argv[2] = test file (utilized by bash file)
    Author: Arjun Subramonian   '''


from machine_learning import MachineLearning
from model_based import ModelBased
from jitter import JitterPredictor
from jacobson import Jacobson
from weightedmedians import RWM
from Mep import MEP 

from sys import argv
import fileinput
import datetime

if __name__ == "__main__":
    predictors = [
        MachineLearning(), 
        ModelBased(), 
        JitterPredictor(open(argv[1], "r")), 
        RWM(),
        MEP(),
        Jacobson(), 
    ]

    filename = argv[2]
    count = 0
    with open(filename, 'r') as f:
        for line in f:
            arr = line.strip().split()
            rtt = float(arr[0])
            try:
                time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S.%f")
            except ValueError:
                time = datetime.datetime.strptime(arr[-1], "%Y-%m-%dT%H:%M:%S")
            for p in predictors:
                p.receive_rtt(rtt, time)
            count += 1
    print '{:20s}'.format(filename),
    for p in predictors:
        print '{:20s}'.format(str(p.lost_packets*100.0/count)),
    print